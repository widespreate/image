function checkNull(dic) {
    for(var key in dic){
        if (dic[key] == '' || dic[key] == undefined){
            $.alert(key);
            return false;
        }
    }
    return true;
}

function getDate(str){
    //去除中文
    var reg =/[\u4e00-\u9fa5]/g;
    str = str.replace(reg,'');

    let date = new Date(str);
    let y = date.getFullYear();
    let m = date.getMonth()+1;
    let d = date.getDate();
    if(m<10){m="0"+m;}
    if(d<10){d="0"+d;}
    return y+"-"+m+"-"+d;
}

//校验身份证号码
function CheckCardValue(idCard) {
    var id = idCard;
    var id_length = id.length;

    if (id_length == 0) {
        //alert("请输入身份证号码!");
        return false;
    }

    if (id_length != 15 && id_length != 18) {
        $.alert("身份证号长度应为15位或18位！");
        return false;
    }

    if (id_length == 15) {
        yyyy = "19" + id.substring(6, 8);
        mm = id.substring(8, 10);
        dd = id.substring(10, 12);

        if (mm > 12 || mm <= 0) {
            //alert("输入身份证号,月份非法！"+mm);
            $.alert("输入身份证号,月份非法！");
            return false;
        }

        if (dd > 31 || dd <= 0) {
            $.alert("输入身份证号,日期非法！");
            return false;
        }

        birthday = yyyy + "-" + mm + "-" + dd;

        if ("13579".indexOf(id.substring(14, 15)) != -1) {
            sex = "1";
        } else {
            sex = "2";
        }
    } else if (id_length == 18) {
        if (id.indexOf("X") > 0 && id.indexOf("X") != 17 || id.indexOf("x") > 0 && id.indexOf("x") != 17) {
            $.alert("身份证中\"X\"输入位置不正确！");
            return false;
        }

        yyyy = id.substring(6, 10);
        if (yyyy > 2200 || yyyy < 1900) {
            $.alert("输入身份证号,年度非法！");
            return false;
        }

        mm = id.substring(10, 12);
        if (mm > 12 || mm <= 0) {
            $.alert("输入身份证号,月份非法！");
            return false;
        }

        dd = id.substring(12, 14);
        if (dd > 31 || dd <= 0) {
            $.alert("输入身份证号,日期非法！");
            return false;
        }

        if (id.charAt(17) == "x" || id.charAt(17) == "X") {
            if ("x" != GetVerifyBit(id) && "X" != GetVerifyBit(id)) {
                $.alert("身份证校验错误，请检查最后一位！");
                return false;
            }

        } else {
            if (id.charAt(17) != GetVerifyBit(id)) {
                $.alert("身份证校验错误，请检查最后一位！");
                return false;
            }
        }

        birthday = id.substring(6, 10) + "-" + id.substring(10, 12) + "-" + id.substring(12, 14);
        if ("13579".indexOf(id.substring(16, 17)) > -1) {
            sex = "1";
        } else {
            sex = "2";
        }
    }

    return true;
}
//15位转18位中,计算校验位即最后一位
function GetVerifyBit(id){
    var result;
    var nNum=eval(id.charAt(0)*7+id.charAt(1)*9+id.charAt(2)*10+id.charAt(3)*5+id.charAt(4)*8+id.charAt(5)*4+id.charAt(6)*2+id.charAt(7)*1+id.charAt(8)*6+id.charAt(9)*3+id.charAt(10)*7+id.charAt(11)*9+id.charAt(12)*10+id.charAt(13)*5+id.charAt(14)*8+id.charAt(15)*4+id.charAt(16)*2);
    nNum=nNum%11;
    switch (nNum) {
        case 0 :
            result="1";
            break;
        case 1 :
            result="0";
            break;
        case 2 :
            result="X";
            break;
        case 3 :
            result="9";
            break;
        case 4 :
            result="8";
            break;
        case 5 :
            result="7";
            break;
        case 6 :
            result="6";
            break;
        case 7 :
            result="5";
            break;
        case 8 :
            result="4";
            break;
        case 9 :
            result="3";
            break;
        case 10 :
            result="2";
            break;
    }
    //document.write(result);
    return result;
}

//校验银行卡号 --6228482918445077176
function luhnCheck(bankno){
    if (bankno.length < 16 || bankno.length > 19) {
        //$("#banknoInfo").html("银行卡号长度必须在16到19之间");
        return false;
    }
    var num = /^\d*$/;  //全数字
    if (!num.exec(bankno)) {
        //$("#banknoInfo").html("银行卡号必须全为数字");
        return false;
    }
    //开头6位
    var strBin="10,18,30,35,37,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,58,60,62,65,68,69,84,87,88,94,95,98,99";
    if (strBin.indexOf(bankno.substring(0, 2))== -1) {
        //$("#banknoInfo").html("银行卡号开头6位不符合规范");
        return false;
    }
    var lastNum=bankno.substr(bankno.length-1,1);//取出最后一位（与luhm进行比较）

    var first15Num=bankno.substr(0,bankno.length-1);//前15或18位
    var newArr=new Array();
    for(var i=first15Num.length-1;i>-1;i--){    //前15或18位倒序存进数组
        newArr.push(first15Num.substr(i,1));
    }
    var arrJiShu=new Array();  //奇数位*2的积 <9
    var arrJiShu2=new Array(); //奇数位*2的积 >9

    var arrOuShu=new Array();  //偶数位数组
    for(var j=0;j<newArr.length;j++){
        if((j+1)%2==1){//奇数位
            if(parseInt(newArr[j])*2<9)
                arrJiShu.push(parseInt(newArr[j])*2);
            else
                arrJiShu2.push(parseInt(newArr[j])*2);
        }
        else //偶数位
            arrOuShu.push(newArr[j]);
    }

    var jishu_child1=new Array();//奇数位*2 >9 的分割之后的数组个位数
    var jishu_child2=new Array();//奇数位*2 >9 的分割之后的数组十位数
    for(var h=0;h<arrJiShu2.length;h++){
        jishu_child1.push(parseInt(arrJiShu2[h])%10);
        jishu_child2.push(parseInt(arrJiShu2[h])/10);
    }

    var sumJiShu=0; //奇数位*2 < 9 的数组之和
    var sumOuShu=0; //偶数位数组之和
    var sumJiShuChild1=0; //奇数位*2 >9 的分割之后的数组个位数之和
    var sumJiShuChild2=0; //奇数位*2 >9 的分割之后的数组十位数之和
    var sumTotal=0;
    for(var m=0;m<arrJiShu.length;m++){
        sumJiShu=sumJiShu+parseInt(arrJiShu[m]);
    }

    for(var n=0;n<arrOuShu.length;n++){
        sumOuShu=sumOuShu+parseInt(arrOuShu[n]);
    }

    for(var p=0;p<jishu_child1.length;p++){
        sumJiShuChild1=sumJiShuChild1+parseInt(jishu_child1[p]);
        sumJiShuChild2=sumJiShuChild2+parseInt(jishu_child2[p]);
    }
    //计算总和
    sumTotal=parseInt(sumJiShu)+parseInt(sumOuShu)+parseInt(sumJiShuChild1)+parseInt(sumJiShuChild2);

    //计算Luhm值
    var k= parseInt(sumTotal)%10==0?10:parseInt(sumTotal)%10;
    var luhm= 10-k;

    if(lastNum==luhm){
        return true;
    }
    else{
        return false;
    }
}

//判断是否登录（未登陆的直接跳转到登陆页面）
function isLogin() {
    var flag = Mdp.isRemoteLogin();
    if (flag == false) {
        $.confirm('您还未登录，是否选择登录？', function () {
            Mdp.openPage('../fuyang_app/login.html');
        }, function () {
            Mdp.openPage('../fuyang_app/charity-resuce.html')
        });
    }
    return flag;

}
function checkPhone(value){
    return /(^0?[1][0-9][0-9]{9}$)/.test(value) || /^((0[1-9]{3})?(0[12][0-9])?[-])?\d{6,8}$/.test(value);
}