function submit() {
    let complainant_name =$("#complainant_name").val();
    let complainant_tel =$("#complainant_tel").val();
    let respondent_name =$("#respondent_name").val();
    let cause =$("#cause").val();
    if (complainant_name === '' || complainant_name === undefined){
        $.alert("请输入投诉人姓名");
        return;
    }
    if (complainant_tel === '' || complainant_tel === undefined){
        $.alert("请输入投诉人联系电话");
        return;
    }else if (!checkPhone(complainant_tel)){
        $.alert("请输入正确的电话号码！");
        return;
    }
    if (respondent_name === '' || respondent_name === undefined){
        $.alert("请输入被投诉人姓名");
        return;
    }
    if (cause === '' || cause === undefined){
        $.alert("请输入投诉原因");
        return;
    }
    $.showLoading();
    Mdp.doAjaxMgr({
            model: "cishan_app_enumerate",
            method: "op_add_complaint",
            complainant_name: complainant_name,
            complainant_tel: complainant_tel,
            respondent_name: respondent_name,
            cause: cause
        },
        function (obj){
            $.hideLoading();
            console.log(obj);
            if (obj.success === "true") {
                $.alert('感谢你对平台的关注和支持。我们会尽快处理你的投诉，通知服务商联系你。','投诉成功',function () {
                    Mdp.openPage('../yl_sjyh_app/home.html');
                })
            }else{
                $.alert(obj.message);
            }
        }
    )
}
function checkPhone(value){
    return /(^0?[1][0-9][0-9]{9}$)/.test(value) || /^((0[1-9]{3})?(0[12][0-9])?[-])?\d{6,8}$/.test(value);
}