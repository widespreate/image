var pk_id = Mdp.getCurrParams().filter.pkId;
$(function () {
    initInfo();
});

function initInfo() {
    let parms = {};
    parms.model = 'cishan_app_enumerate';
    parms.data = 'data_charity_project';
    parms.pk_id = pk_id;

    Mdp.doAjaxData({
            model: "cishan_app_enumerate",
            data: 'data_charity_project',
            pk_id: pk_id,
            async: true},
            function(obj) {
            console.log(obj);
            let data = obj.data;
            if (obj.success == 'true') {
                $("#pic_url").attr('src',data.projectpic_url);
                $("#xzqh").text(data.xzqh);
                $("#projectName").text(data.projectname);
                $("#projectType").text(data.projecttype);
                $("#director").text(data.director);
                $("#contact").text(data.contact);
                $("#officeLocation").text(data.officelocation);
                $("#ifLongTerm").text(data.iflongterm);
                if (data.iflongterm == '是'){
                    $("#effectiveStartDate").parent().hide();
                    $("#effectiveEndDate").parent().hide();
                }else {
                    $("#effectiveStartDate").text(data.effectivestartdate);
                    $("#effectiveEndDate").text(data.effectiveenddate);
                }
                $("#situationDescription").text(data.situationdescription);

            }else{
                $.alert(obj.message);
            }
    });
}