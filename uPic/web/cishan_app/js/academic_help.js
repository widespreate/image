$(function () {
    //判断是否登录
    if (isLogin() == false) {
        return;
    }
    Mde.loadFromTemp();
})
function submit(){
    var xzqh = $("#xzqh").attr("node");
    var qhmc = $("#xzqh").val();
    var csqr = $("#csqr").val();
    var csqrsfz = $("#csqrsfz").val();
    var cxslx = $("#cxslx").attr("data-values");
    var clxdh = $("#clxdh").val();
    var cjhrxm = $("#cjhrxm").val();
    var cjhrsfz = $("#cjhrsfz").val();
    let cxxlx = $("#cxxlx").attr("data-values");
    var cnj = $("#cnj").attr("data-values");
    var cxxmc = $("#cxxmc").val();
    var ckhyh = $("#ckhyh").attr("data-values");
    var ckhr = $("#ckhr").val();
    var cyhkh = $("#cyhkh").val();
    var cjzrq = $("#cjzrq").val();

    cjzrq = getDate(cjzrq);

    let dic = {
        '请选择行政区划':xzqh,
        '请输入学生姓名':csqr,
        '请输入学生身份证':csqrsfz,
        '请选择学生类型':cxslx,
        '请输入联系电话':clxdh,
        '请输入监护人姓名':cjhrxm,
        '请输入监护人身份证':cjhrsfz,
        '请选择学校类型': cxxlx,
        '请选择年级': cnj,
        '请输入学校名称':cxxmc,
        '请选择开户银行':ckhyh,
        '请输入开户人':ckhr,
        '请输入银行账号':cyhkh,
        '请选择救助日期':cjzrq
    };

    if (!checkNull(dic)){
        return;
    }
    if (!CheckCardValue(csqrsfz)){
        $.alert('请输入正确的学生身份证');
        return;
    }
    if (!checkPhone(clxdh)){
        $.alert('请输入正确的联系电话');
        return;
    }
    if (!CheckCardValue(cjhrsfz)){
        $.alert('请输入正确的监护人身份证');
        return;
    }
    if (!luhnCheck(cyhkh)){
        $.alert('请输入正确的银行账号');
        return;
    }

    $.showLoading();
    Mdp.doAjaxMgr({
            async:false,
            model: "cishan_app_enumerate",
            method: "op_add_academic",
            sys_xzqh: xzqh,
            qhmc: qhmc,
            xzqh: xzqh,
            csqr: csqr,
            csqrsfz: csqrsfz,
            cxslx: cxslx,
            clxdh: clxdh,
            cjhrxm: cjhrxm,
            cjhrsfz: cjhrsfz,
            cxxlx: cxxlx,
            cnj: cnj,
            cxxmc: cxxmc,
            ckhyh: ckhyh,
            ckhr: ckhr,
            cyhkh: cyhkh,
            cjzrq: cjzrq
        },
        function (obj){
            $.hideLoading();
            console.log(obj);
            if (obj.success === "true") {
                $.alert('申请成功',function () {
                    Mdp.openPage('../fuyang_app/charity-resuce.html');
                })
            }else{
                $.alert(obj.message);
            }
        }
    )
}



function changeSchool(){
    let cnj = $("#cnj").attr("data-values");
    if(1 <= cnj && 6 >= cnj){
        $("#cxxlx").val('小学');
        $("#cxxlx").attr("data-values",'1');
    }else if(7 <= cnj && 9 >= cnj){
        $("#cxxlx").val('初中');
        $("#cxxlx").attr("data-values",'2');
    }else if(10 <= cnj && 12 >= cnj){
        $("#cxxlx").val('高中');
        $("#cxxlx").attr("data-values",'3');
    }else if(13 <= cnj && 20 >= cnj){
        $("#cxxlx").val('大学');
        $("#cxxlx").attr("data-values",'4');
    }
}