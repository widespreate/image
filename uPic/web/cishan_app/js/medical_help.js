var pk_id = Mdp.uuid();

$(function () {
    //判断是否登录
    if (isLogin() == false) {
        return;
    }
})
function submit(){
    var xzqh = $("#xzqh").attr("node");
    var qhmc = $("#xzqh").val();
    var csqr = $("#csqr").val();
    var csqrsfz = $("#csqrsfz").val();
    var chzxm = $("#chzxm").val();
    var chzsfz = $("#chzsfz").val();
    var cbzlx = $("#cbzlx").attr("data-values");
    var cjtzrk = $("#cjtzrk").val();
    var ckhyh = $("#ckhyh").attr("data-values");
    var ckhr = $("#ckhr").val();
    var cyhkh = $("#cyhkh").val();
    var cjzlx = $("#cjzlx").attr("data-values");
    var sfzfyj_picurl = $("#sfzfyj_imgs").val(); //身份证复印件图片
    var xgcl_picurl = $("#xgcl_imgs").val(); //相关材料图片

    let dic = {
        '请选择行政区划':xzqh,
        '请输入申请人姓名':csqr,
        '请输入申请人身份证':csqrsfz,
        '请输入户主姓名':chzxm,
        '请输入户主身份证':chzsfz,
        '请选择病种类型':cbzlx,
        '请输入家庭总人口':cjtzrk,
        '请选择开户银行':ckhyh,
        '请输入开户人':ckhr,
        '请输入银行账号':cyhkh,
        '请选择救助类型':cjzlx,
        '请上传身份证复印件':sfzfyj_picurl,
        '请上传相关材料':xgcl_picurl
    };
    if (!checkNull(dic)){
        return;
    }
    if (!CheckCardValue(csqrsfz)){
        $.alert('请输入正确的申请人身份证');
        return;
    }
    if (!CheckCardValue(chzsfz)){
        $.alert('请输入正确的户主身份证');
        return;
    }
    if (!luhnCheck(cyhkh)){
        $.alert('请输入正确的银行账号');
        return;
    }
    $.showLoading();
    Mdp.doAjaxMgr({
            async:false,
            model: "cishan_app_enumerate",
            method: "op_add_medical",
            pk_id: pk_id,
            sys_xzqh: xzqh,
            qhmc: qhmc,
            xzqh: xzqh,
            csqr: csqr,
            csqrsfz: csqrsfz,
            chzxm: chzxm,
            chzsfz: chzsfz,
            cbzlx: cbzlx,
            cjtzrk: cjtzrk,
            ckhyh: ckhyh,
            ckhr: ckhr,
            cyhkh: cyhkh,
            cjzlx: cjzlx
        },
        function (obj){
            console.log(obj);
            uploadPic(sfzfyj_picurl,'file_sfz')
            uploadPic(xgcl_picurl,'file_other')
            $.hideLoading();
            if (obj.success === "true") {
                $.alert('申请成功',function () {
                    Mdp.openPage('../fuyang_app/charity-resuce.html');
                })
            }else{
                $.alert(obj.message);
            }
        }
    )
}
//上传附件
function uploadPic(picurlAll,fupcode){
    var arr = picurlAll.split(",");
    var fname = '';
    for (var i = 0; i < arr.length; i++) {
        var picurl = arr[i];
        fname = picurl.substr(picurl.lastIndexOf("/") + 1);
        console.log("开始-上传图片"+i);
        Mdp.doAjaxMgr({async:false, model: 'cishan_app_enumerate', method: 'op_add_medical_pic',
                pk_id: pk_id, fname: fname, fpath : picurl,fupcode: fupcode},
            function (obj) {
                if(obj.success == 'true'){
                    console.log(obj.message);
                    console.log("成功-上传图片。")
                }else{
                    $.alert(obj.message);
                    return;
                }
            });
    }
}


