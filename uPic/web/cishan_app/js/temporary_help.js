$(function () {
    //判断是否登录
    if (isLogin() == false) {
        return;
    }
    Mde.loadFromTemp();
})
function submit(){
    var xzqh = $("#xzqh").attr("node");
    var qhmc = $("#xzqh").val();
    var csqr = $("#csqr").val();
    var csqrsfz = $("#csqrsfz").val();
    var chzxm = $("#chzxm").val();
    var chzsfz = $("#chzsfz").val();
    var cjtzrk = $("#cjtzrk").val();
    var ckhyh = $("#ckhyh").attr("data-values");
    var ckhr = $("#ckhr").val();
    var cyhkh = $("#cyhkh").val();
    var cjzrq = $("#cjzrq").val();

    cjzrq = getDate(cjzrq);

    let dic = {
        '请选择行政区划':xzqh,
        '请输入申请人姓名':csqr,
        '请输入申请人身份证':csqrsfz,
        '请输入户主姓名':chzxm,
        '请输入户主身份证':chzsfz,
        '请输入家庭总人口':cjtzrk,
        '请选择开户银行':ckhyh,
        '请输入开户人':ckhr,
        '请输入银行账号':cyhkh,
        '请选择救助日期':cjzrq
    };

    if (!checkNull(dic)){
        return;
    }
    if (!CheckCardValue(csqrsfz)){
        $.alert('请输入正确的申请人身份证');
        return;
    }
    if (!CheckCardValue(chzsfz)){
        $.alert('请输入正确的户主身份证');
        return;
    }
    if (!luhnCheck(cyhkh)){
        $.alert('请输入正确的银行账号');
        return;
    }

    $.showLoading();
    Mdp.doAjaxMgr({
            async:false,
            model: "cishan_app_enumerate",
            method: "op_add_temporary",
            sys_xzqh: xzqh,
            qhmc: qhmc,
            xzqh: xzqh,
            csqr: csqr,
            csqrsfz: csqrsfz,
            chzxm: chzxm,
            chzsfz: chzsfz,
            cjtzrk: cjtzrk,
            ckhyh: ckhyh,
            ckhr: ckhr,
            cyhkh: cyhkh,
            cjzrq: cjzrq
        },
        function (obj){
            $.hideLoading();
            console.log(obj);
            if (obj.success === "true") {
                $.alert('申请成功',function () {
                    Mdp.openPage('../fuyang_app/charity-resuce.html');
                })
            }else{
                $.alert(obj.message);
            }
        }
    )
}
