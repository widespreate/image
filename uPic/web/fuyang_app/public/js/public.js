//获取当前定位信息
function getlocation(){
    var  curr_lat = "";//纬度
    var	 curr_lng = "";//经度
    var	 curr_addr = "";//地址
    var   dwztB = true; //定位状态
    var geo = new BMap.Geolocation();
    //获取当前所在位置的经纬度
    geo.getCurrentPosition(function(r){
        if (this.getStatus() == BMAP_STATUS_SUCCESS){
            curr_lat = r.point.lat;//纬度
            curr_lng = r.point.lng;//经度

            var map = new BMap.Map("allmap");
            var point = new BMap.Point(curr_lng, curr_lat);
            var gc = new BMap.Geocoder();
            gc.getLocation(point, function(rs){
                var addComp = rs.addressComponents;
                console.log(addComp.province + "|" + addComp.city + "|" + addComp.district + "|" + addComp.street + "|" + addComp.streetNumber);
            });
        }else{
            dwztB =false;
        }
    });
}

//判断是否登录
function isLogin() {
    var flag = Mdp.isRemoteLogin();
    if (flag == false) {
         $.confirm('您还未登录，是否选择登录？', function () {
            Mdp.openPage('login.html');
        }, function () {
            //取消操作
            Mdp.openPage('home_index.html');
        });
    } else {
        var user = localStorage.getItem("user");
        if (user == '' || user == undefined) {
            $.confirm('您还未登录，是否选择登录？', function () {
                Mdp.openPage('login.html');
            }, function () {
                //取消操作
                Mdp.openPage('home_index.html');
            });
        }
    }
    return flag;
}

//最下面切换页面 判断是否登录
function  GoPage(index){
    if (isLogin() == false) {
        return;
    }else{
        if(index=="1"){ //消息
            Mdp.openPage('message_index.html')
        }else if(index=="2"){ //签到/参与（需要登录和人员控制）
            var storage = window.localStorage;
            var user = JSON.parse(storage.getItem("user"));
             if(user.utype == "1" || user.utype == "1001"){
                alert("只针对个人账户开放！");
            }else{
                Mdp.openPage('sign_in_coded_input.html');
            }

        }else if(index=="3"){
                Mdp.openPage('organization_my_organization.html');
         }else if(index=="4"){
            Mdp.openPage('personal_index.html');
        } else if(index=="fxy") {
            var storage = window.localStorage;
            var user = JSON.parse(storage.getItem("user"));
            if(user.utype == "1" || user.utype == "1001"){
                $.alert("只针对个人账户开放！");
            }else{
                Mdp.openPage('neighborhood_give_wish.html');
            }
        }

    }



}


/**
 *    获取当前时间，格式YYYY-MM-DD HH:mm:ss
 **/
function getNowTime() {
    var date = new Date();
    var seperator1 = "-";
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var strDate = date.getDate();
    var hours = date.getHours();
    var minute = date.getMinutes();
    var second = date.getSeconds();
    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }
    if (hours >= 1 && hours <= 9) {
        hours = "0" + hours;
    }
    if (minute >= 1 && minute <= 9) {
        minute = "0" + minute;
    }
    if (second >= 1 && second <= 9) {
        second = "0" + second;
    }

    var currentdate = year + seperator1 + month + seperator1 + strDate + " " + hours + ":" + minute + ":" + second;
    console.log("currentdatetime==== "+currentdate)

    return currentdate;
}


/**
 *    获取当前时间，格式YYYY-MM-DD
 **/
function getNowDate() {
    var date = new Date();
    var seperator1 = "-";
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var strDate = date.getDate();
    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }

    var currentdate = year + seperator1 + month + seperator1 + strDate;
    console.log("currentdate==== "+currentdate)

    return currentdate;

}
/**
 *    调用类获取当前时间，格式YYYY-MM-DD
 **/
function getCurrentDate() {
    var currentdate;
    Mdp.doAjaxMgr({
        async: false,
        model: "getCurrentDateTime",
        method: "op_current_date"
    },function (obj) {
        console.log(obj);
        if (obj.success == "false") {
            $.alert(obj.message);
            return;
        }else{
            currentdate=obj.data.CurrentDate;
        }

    },false);
    return currentdate;

}

/**
 *    调用类获取当前时间，格式YYYY-MM-DD hh:mm:ss
 **/
function getCurrentTime() {
    var currentDateTime;
    Mdp.doAjaxMgr({
        async: false,
        model: "getCurrentDateTime",
        method: "op_current_date"
    },function (obj) {
        console.log(obj);
        if (obj.success == false) {
            $.alert(obj.message);
            return;
        }else{
            currentDateTime = obj.data.CurrentDateTime;
        }

    },false);
    return currentDateTime;

}

function goPageNode(node) {

    if (node == 'zyz') { //成为志愿者
        Mdp.openPage('personal_volunteer.html');
    } else if (node == 'xm') { //找项目
        Mdp.openPage('project_list.html');
    } else if (node == 'zz') { //找组织
        Mdp.openPage('organization_find_organization.html')
    }


}

//区划必须选择社区
function  judgeQhLength(id) {
    var qhid= $("#"+id).attr("node")
    if(qhid.length<10){
        $.alert("区划请选择到社区")
        $("#"+id).attr("node","")
        $("#"+id).val("")
        return
    }

}


//时间控件
function isWhichTime(vel) {
    let years = [];
    let mouth =[];
    let date = [];
    let hours = [];
    let minite = [];
    let seconds = [];
    if(!years.length&&!mouth.length) {
        for(let i = 1889; i <= 2030; i++) {
            years.push({
                label: i +"年" ,
                value: i
            });
        }
    }
    if (!mouth.length) {
        mouth = costomDatePicker(mouth,1,13,"月" )
    }
    if(!date.length) {
        date = costomDatePicker(date,1,32,"日" )
    }
    if(!hours.length) {
        hours=costomDatePicker(hours,0,24,"时")
    }
    if(!minite.length) {
        minite=costomDatePicker(minite,0,60,"分")
    }
    if(!seconds.length) {
        seconds=costomDatePicker(seconds,0,60,"秒")
    }

    weui.picker(years,mouth,date,hours,minite,seconds,{
        //end:2030,
        id: new Date(),//让每一个点击弹框得都有一个不一样得id，这样就避免了共用一个弹框得时候，打开得日期是上一次另外一个得日期回显
        defaultValue: [new Date().getFullYear(), new Date().getMonth()+1, new Date().getDate(),new Date().getHours(),new Date().getMinutes(),new Date().getSeconds()],
        onConfirm: result=> {
            //result 是数组，返回得是当前选中得数，如[{lable:'2019年,value:2019},{lable:'2月,value:2}，{lable:'2号,value:2}]
            if (result.length){
                //yyyy-mm-dd hh:mm:ss
                let time=result[0].value+'-'+result[1].value+'-'+result[2].value+' '+result[3].value+':'+result[4].value+':'+result[5].value;
                console.log('time',time);
                $(vel).children('input').val(time);
            }
        }
    });
}


//自定义日期:月时分秒
function costomDatePicker(years,startTime,endTime,str){
    for(let j = startTime; j < endTime; j++) {
        years.push({
            label: ('' + j).length === 1 ? '0'+j + str : '' + j + str,
            value: ('' + j).length === 1 ? '0'+j : '' + j ,
        });
    }
    return years;
}


//获取消息总数
function sunMessageNum() {
    document.getElementById("xxnum").style.display = "none";
    var xxnum = 0;
    Mdp.doAjaxData({model: 'sht_app_enumerate', data: 'data_xxtz_sum', xxxlx: '1', async: false}, function (obj) {
        var num = obj.data.sum1;
        xxnum += Number(num);
    });
    Mdp.doAjaxData({model: 'sht_app_enumerate', data: 'data_xxtz_sum', xxxlx: '2', async: false}, function (obj) {
        var num = obj.data.sum1;
        xxnum += Number(num);
    });
    Mdp.doAjaxData({model: 'sht_app_enumerate', data: 'data_xxtz_sum', xxxlx: '3', async: false}, function (obj) {
        var num = obj.data.sum1;
        xxnum += Number(num);
    });
    Mdp.doAjaxData({model: 'sht_app_enumerate', data: 'data_xxtz_sum', xxxlx: '4', async: false}, function (obj) {
        var num = obj.data.sum1;
        xxnum += Number(num);
    });
if (xxnum == 0){
        document.getElementById("xxnum").style.display = "none";
    }else{
        document.getElementById("xxnum").style.display = "block";
    }
    $("#xxnum").text(xxnum);
}


//根据角色类型获取用户信息保存到localStorage
function loginAfter(username,new_password) {
    console.log(username+"=="+new_password);
    Mdp.doAjax(Mdp.globals.appUrl + "/main/authorize.as?method=login&execAjax=true&mobileLogin=true",
        {username: username, password: new_password},
        function (obj) {

            //根据角色类型获取用户信息保存到localStorage
            roleAssignment(obj);
        },
        function (XMLHttpRequest, textStatus, errorThrown) {
            alert("用户名或密码错误！");
            return;
        },
        true
    );
}


//获取当前用户信息，进行页面跳转
function getBdUserInfo() {
    doAjaxMgr({ model: "shtWechatLoginManager", method:"getBdUserInfo"},
        function(obj) {
            console.log("用户信息:",obj);
            if(obj.success == "true") {

                //根据角色类型获取用户信息保存到localStorage
                roleAssignment(obj);
            }else{
                alert("获取用户信息失败！");
            }
        });
}

//根据角色类型获取用户信息保存到localStorage
function roleAssignment(obj) {
    console.log('obj --- > ', obj);
    if (obj.success == 'true') {
        //根据utype区分登录用户角色
        let utype = obj.data.type;
        if (utype == '5002' || utype == '5000' || utype == '5003' || utype == '5004') { //志愿者或者会员
            Mdp.doAjaxData({model: "sht_app_enumerate", data: "data_rygl_info_login", async: false}, function (obj) {
                if (obj.success == 'true') {
                    localStorage.setItem("user", JSON.stringify(obj.data));
                    Mdp.openPage('home_index.html');
                }
            }, false);

        } else if (utype == '1001') {  //组织账户
            Mdp.doAjaxData({model: "sht_app_enumerate", data: "data_shzz_info_login", async: false}, function (obj) {
                if (obj.success == 'true') {
                    localStorage.setItem("user", JSON.stringify(obj.data));
                    Mdp.openPage('home_index.html');
                }
            }, false);

        } else {  //社区、民政账号
            Mdp.doAjaxData({model: "sht_app_enumerate", data: "data_sq_my_info", async: false}, function (obj) {
                if (obj.success == 'true') {
                    localStorage.setItem("user", JSON.stringify(obj.data));
                    Mdp.openPage('home_index.html');
                }
            }, false);
        }
    } else {
        $.alert(obj.message || obj.msg);
    }
}


//百度地图SDK辅助定位
function sdkDw() {
    var map = new BMap.Map("allmap");//创建Map实例，注意页面中一定要有个id为allmp的div
    var point = new BMap.Point(121.239301,28.14135);//创建定坐标
    map.centerAndZoom(point,12); //初始化地图,设置中心点坐标和地图级别

    var geolocation = new BMap.Geolocation();
    var gc = new BMap.Geocoder();//创建地理编码器
    // 开启SDK辅助定位
    geolocation.enableSDKLocation();
    geolocation.getCurrentPosition(function(r){
        if(this.getStatus() == BMAP_STATUS_SUCCESS){
            var mk = new BMap.Marker(r.point);
            map.addOverlay(mk);
            map.panTo(r.point);
            _curr_lng = r.point.lng;
            _curr_lat = r.point.lat;

            $.alert('您的位置：' + r.point.lng + ',' + r.point.lat);
            var pt = r.point;
            map.panTo(pt);//移动地图中心点

            gc.getLocation(pt, function(rs){
                var addComp = rs.addressComponents;
                //alert(addComp.city);
                $.alert(addComp.province + addComp.city + addComp.district + addComp.street + addComp.streetNumber);
            });

        }
        else {
            _curr_lng = "121.239301";
            _curr_lat = "28.14135";
            $.alert('failed'+this.getStatus());
        }
    });
}



//百度地图浏览器定位
function llqDw() {

    var map = new BMap.Map("allmap");
    var point = new BMap.Point(121.239301,28.14135);
    map.centerAndZoom(point,12);

    var geolocation = new BMap.Geolocation();
    geolocation.getCurrentPosition(function(r){
        if(this.getStatus() == BMAP_STATUS_SUCCESS){
            var mk = new BMap.Marker(r.point);
            map.addOverlay(mk);
            map.panTo(r.point);
            _curr_lng = r.point.lng;
            _curr_lat = r.point.lat;
            //$.alert('您的位置：'+r.point.lng+','+r.point.lat);
        }
        else {
            _curr_lng = "121.239301";
            _curr_lat = "28.14135";
            //$.alert('failed'+this.getStatus());
        }
    });
}
