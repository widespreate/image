function loadScript(url, callback) {
    var script = document.createElement("script");
    script.type = "text/javascript";
    if(typeof(callback) != "undefined"){
        if (script.readyState) {
            script.onreadystatechange = function () {
                if (script.readyState == "loaded" || script.readyState == "complete") {
                    script.onreadystatechange = null;
                    callback();
                }
            };
        } else {
            script.onload = function () {
                callback();
            };
        }
    }
    script.src = url;
    document.body.appendChild(script);
}

//lazyImg
function ljz(id){
    var lazyLoadImg = new LazyLoadImg({
        el: document.querySelector('#' + id),
        mode: 'default', //默认模式，将显示原图，diy模式，将自定义剪切，默认剪切居中部分
        time: 300, // 设置一个检测时间间隔
        complete: true, //页面内所有数据图片加载完成后，是否自己销毁程序，true默认销毁，false不销毁
        position: { // 只要其中一个位置符合条件，都会触发加载机制
            top: 0, // 元素距离顶部
            right: 0, // 元素距离右边
            bottom: 0, // 元素距离下面
            left: 0 // 元素距离左边
        },
        before: function () { // 图片加载之前执行方法

        },
        success: function (el) { // 图片加载成功执行方法
            el.classList.add('success')
        },
        error: function (el) { // 图片加载失败执行方法
            el.src = './images/error.png'
        }
    })
}

//拍照弹出
function photo(pageId){
    var popPhoto = pageId.find(".popPhoto");
    var popPhotoH = popPhoto.height();
    popPhoto.css("bottom",-popPhotoH);
    pageId.find("#photoBtn").tap(function(){
        pageId.find(".yz-cover").show();
        popPhoto.animate({"bottom":0},200)
    });
    pageId.find(".photoCancel").tap(function(){
        pageId.find(".yz-cover").hide();
        popPhoto.animate({"bottom":-popPhotoH},200)
    })
}
$(document).on("tap",".page",function(){
    //alert(1)
    if($('.topToolBox').css('display') == 'block'){
        $('.topToolBox').hide();
    }
});
$(document).on("tap","#moreTool",function(){
    //event.stopPropagation();
    var x =$(this).offset().left;
    var y =$(this).offset().top;
    var headH = $(".header").height();
    $(".topToolBox").css({
        "top":46
    });
    $(".topToolBox").toggle();
    //console.log("2")
    return false;
});

$(function(){
    var ctH = $(".content").height()-112;
    var mtop = (ctH-$(".listNothing").height()-130)/2;
    $(".listNothing").height(ctH);

    //pop
    var wH = $(window).height();
    var wW = $(window).width();
    $(".screenPop").css({
        "height":wH,
        left:wW
    });
    var search = $(".screenPop");
    var scH = search.height()-search.find(".header").height()-58;
    $(".searchCon").height(scH);
    $(".searchBack").tap(function(){
        $(".screenPop").animate({left:wW},300);
        //$("#search").hide();
    });

    $('.js-category').click(function(){
        $parent = $(this).parent('li');
        if($parent.hasClass('js-show')){
            $parent.removeClass('js-show');
            $(this).children('i').removeClass('icon-35').addClass('icon-74');
        }else{
            $parent.siblings().removeClass('js-show');
            $parent.addClass('js-show');
            $(this).children('i').removeClass('icon-74').addClass('icon-35');
            $parent.siblings().find('i').removeClass('icon-35').addClass('icon-74');
        }
    });

    screenPop("sildeAdd");
});

function screenPop(id){
    $("#"+id).tap(function(){
        //$(this).parent(".topToolBox").hide();
        var vId = $(this).attr("v-id");
        //console.log(vId);
        $("#"+vId).animate({left:0},300);
    });
}






