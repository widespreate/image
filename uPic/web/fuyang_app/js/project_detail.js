var pk_id = Mdp.getCurrParams().filter.pk_id;
var sgin = Mdp.getCurrParams().filter.sgin;
var user = JSON.parse(localStorage.getItem("user"));
console.log(pk_id)
console.log(sgin)
$(function () {
    //判断是否登陆
    isLogin();
    //查询所有页面信息
    getInfo();
    //判断按钮状态
    buttonShow();
})

//判断是否登录（未登陆的直接跳转到登陆页面）
function isLogin() {
    console.log('1')
    var flag = Mdp.isRemoteLogin();
    if (flag == false) {
        $.confirm('您还未登录，是否选择登录？', function () {
            localStorage.setItem("is_qrcode", "true");
            Mdp.openPage('../fuyang_app/login.html');
        }, function () {
            Mdp.openPage('../fuyang_app/index.html')
        });
    }
    return flag;
}

//加载页面信息
function getInfo() {
    // data_complaintDetails
    Mdp.doAjaxRowData({
        model: 'efamily_app', data: 'data_gyctDetails',
        pk_id: pk_id, async: false
    }, function (obj) {
        var data = obj.data_gyctDetails.list[0];
        // console.log(data)
        $("#rxmmc").html(data.rxmmc);
        //项目名称
        appProject = data.rxmmc;
        $("#rpgjd").html(data.rpgjd);
        $("#rxmsbdw").html(data.rxmsbdw);
        $("#rxmsbrq").html(data.rxmsbrq);
        $("#rxmksrq").html(data.rxmksrq);
        //项目开始时间
        appStart = data.rxmksrq;
        $("#rxmjsrq").html(data.rxmjsrq);
        //项目结束时间
        appEnd = data.rxmjsrq;
        $("#rxmfzr").html(data.rxmfzr);
        $("#rxmzxr").html(data.rxmzxr);
        $("#rxmsyr").html(data.rxmsyr);
        $("#rxmsyrs").html(data.rxmsyrs);
        sumPeople = data.rxmsyrs
        $("#rxmzje").html(parseInt(data.rxmzje) + " 元");
        $("#rzzje").html(parseInt(data.rzzje) + " 元");
        $("#rzcje").html(parseInt(data.rzcje) + " 元");
        $("#rxmly").html(data.rxmly);
        $("#rsj").html(parseInt(data.rsj) + " 元");
        $("#rxmssly").html(data.rxmssly);
        $("#rfzr").html(data.rfzr);
        $("#rfzrdh").html(data.rfzrdh);
        $("#rfzrsj").html(data.rfzrsj);
        $("#rfzryx").html(data.rfzryx);
        $("#rlxr").html(data.rlxr);
        $("#rlxrdh").html(data.rlxrdh);
        $("#rlxrsj").html(data.rlxrsj);
        $("#rlxryx").html(data.rlxryx);
    })
}

//判断按钮状态
function buttonShow() {
    console.log(user.userid)
    Mdp.doAjaxMgr({model: "projectDetailManager", method: "getIsApplyAndSum", "fkGyctRecord": pk_id}, function (obj) {
        console.log(obj)
        if (obj.success === "true") {
            const submit = $("#doJoin");
            isApply = obj.data.isApply;
            sum = obj.data.sum;
            if (isApply === '0') {
                submit.css("background", "#ccc");
                submit.html("取消报名");
            } else if (sum >= sumPeople) {
                submit.css("background", "#ccc");
                submit.html("人数已满");
            } else {
                submit.css("background", "#2b88ff");
                submit.html("立即报名");
            }
        }
    })
}

//点击报名或者取消报名按钮
function submit() {
    let bol = true;
    let ss = false;
    //取消报名
    if (isApply === '0') {
        $("#submit").attr("disabled", bol);
        Mdp.doAjaxMgr({model: "projectDetailManager", method: "cancelReg", "fkGyctRecord": pk_id}, function (obj) {
            console.log(obj)
            if (obj.success === "true") {
                $.alert("取消报名成功！", function (obj) {
                    location.reload();
                    $("#submit").attr("disabled", ss);
                });
            }
        })
    } else if (sum >= sumPeople) {
        $.alert("报名人数已满！", function (obj) {
        });
    } else {
        //判断是否实名认证和绑定单位
        //判断是否实名
        if (user.idcard == "") {
            $.confirm('您还未实名认证，是否选择实名认证？', function () {
                Mdp.openPage('../fuyang_app/my_info.html');
            }, function () {
                //取消操作
                return;
            });
            return;
        }
        if (user.udw == "" || user.uzw == "") {
            $.confirm('请绑定您的单位和职务', function () {
                Mdp.openPage('../fuyang_app/my_info.html');
            }, function () {
                //取消操作
                return;
            });
            return;
        }
        //报名
        $("#submit").attr("disabled", bol);
        Mdp.doAjaxMgr({
            model: "projectDetailManager",
            method: "signUp",
            "fkGyctRecord": pk_id,
            "appProject": appProject,
            "appStart": appStart,
            "appEnd": appEnd
        }, function (obj) {
            console.log(obj)
            if (obj.success === "true") {
                $.alert("报名成功！", function (obj) {
                    location.reload();
                    $("#submit").attr("disabled", ss);
                });
            }
        })
    }
}

//返回按钮
function back() {
    if (sgin === '1') {
        Mdp.backPage('my_project.html');
    }else{
        Mdp.backPage('project_list.html');
    }
}