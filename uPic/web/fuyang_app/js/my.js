
var mpage = {
    create: function(params) {
        if (isLogin() == false) {
            return;
        }

        var user = JSON.parse(localStorage.getItem("user"));

        loadMyInfo(user);
        showNumber();
    },
    show: function(params) {

    },
    close: function(params) {

    }
}



//加载我的信息
function loadMyInfo(obj) {
    var user = JSON.parse(localStorage.getItem("user"));
    $("#uxm").html(obj.uxm);
    var zyzpk = window.localStorage.getItem("zyzpk"); //志愿者主键
    var utype = obj.utype;
    setTx(utype, obj.idcard);//设置头像
    var usfz = obj.idcard;
    if (utype == '1') {
        $("#utype").html('管理员');
    } else if (utype == '1102') {
        $("#utype").html('运营商工作人员');
    } else if (utype == '1103 ') {
        $("#utype").html('运营商管理人员');
    } else if (utype == '1104  ') {
        $("#utype").html('运营商服务人员');
    } else if (utype == '1101') {
        $("#utype").html('运营商');
    } else if (utype == '1201' || utype == '1019') {
        $("#utype").html('社会组织');
    } else if (utype == '1301') {
        $("#utype").html('个人账户');
        if (obj.uxm == '') {
            $("#uxm").html(obj.username);
        }
        $("#sjyh").show();
        $("#csjz").show();
        $("#sgrc").show();
        $(".mt").show();
        $(".type span").show();
        Mdp.doAjaxMgr({
            model: "personTimeAndIntegralManager",
            method: "queryTimeAndIntegral",
            userid: user.userid,
            zyzpk: zyzpk
        },function (obj) {
            if(obj.success != "true"){
                $.alert(obj.message);
            }else{
                $("#point").html(obj.data.integral_balance);
                $("#time").html(obj.data.fwsc);
            }
        },false)
    } else if (utype == '1202') {
        $("#utype").html('组织工作人员');
    } else {
        $("#utype").html('组织服务人员');
    }
    if (utype == '1301' && usfz == '') {
        $("#smrz").show();
    }
}


//头像设置
function setTx(utype, usfz) {
    var lpicurl= "";
    if (utype == '1'){//sr
        lpicurl = "images/img_logo2.png";
    }else if (utype == '1102' || utype == '1103' || utype == '1104'){//运营商工作人员头像：服务人员、工作人员
        Mdp.doAjaxRowData({model:"efamily_app", data:"data_yys_ry_pic", async: false},
            function(obj) {
                if (obj.success == "false") {
                    alert(obj.message); return;
                }
                if (obj.data_yys_ry_pic.list.length > 0){
                    lpicurl = obj.data_yys_ry_pic.list[0].lpicurl;
                }
            }, false
        );
        // 设置头像
        if (lpicurl == "" && usfz != ""){
            let xb = Mde.getIdCardInfo(usfz, 2);
            if(xb=="男"){
                lpicurl ="../yl_sjyh_app/images/boy.png";
            }else if(xb=="女"){
                lpicurl ="../yl_sjyh_app/images/girl.png";
            }
        }
    }else if (utype == '1101') {//运营商
        Mdp.doAjaxRowData({model:"efamily_app", data:"data_yys_pic", async: false},
            function(obj) {
                if (obj.success == "false") {
                    alert(obj.message); return;
                }
                if (obj.data_yys_pic.list.length > 0){
                    lpicurl = obj.data_yys_pic.list[0].lpicurl;
                }
            }, false
        );
        if (lpicurl == ""){
            lpicurl = "images/organization.png";
        }
    }else if (utype == '1201' || utype == '1019') {//社会组织
        Mdp.doAjaxRowData({model:"efamily_app", data:"data_fws_pic", async: false},
            function(obj) {
                if (obj.success == "false") {
                    alert(obj.message); return;
                }
                if (obj.data_fws_pic.list.length > 0){
                    lpicurl = obj.data_fws_pic.list[0].lpicurl;
                }
            }, false
        );
        if (lpicurl == ""){
            lpicurl = "images/organization.png";
        }
    }else if (utype == '1301'){//个人账户
        // 获取个人账户（老年人）的头像
        Mdp.doAjaxRowData({model:"efamily_app", data:"data_older_pic", async: false},
            function(obj) {
                if (obj.success == "false") {
                    alert(obj.message); return;
                }
                if (obj.data_older_pic.list.length > 0){
                    lpicurl = obj.data_older_pic.list[0].lpicurl;
                }
            }, false
        );
        if (lpicurl == "" && usfz != ""){
            let xb = Mde.getIdCardInfo(usfz, 2);
            if(xb=="男"){
                lpicurl ="../yl_sjyh_app/images/boy.png";
            }else if(xb=="女"){
                lpicurl ="../yl_sjyh_app/images/girl.png";
            }
        }
    }else{//服务商工作人员、组织服务人员
        Mdp.doAjaxRowData({model:"efamily_app", data:"data_fws_ry_pic", async: false},
            function(obj) {
                if (obj.success == "false") {
                    alert(obj.message);
                    return;
                }
                if (obj.data_fws_ry_pic.list.length > 0){
                    lpicurl = obj.data_fws_ry_pic.list[0].lpicurl;
                }

            }, false
        );

        if (lpicurl == "" && usfz != ""){
            let xb = Mde.getIdCardInfo(usfz, 2);
            if(xb=="男"){
                lpicurl ="images/fw_default.png";
            }else if(xb=="女"){
                lpicurl ="images/fw_defaultMale.png";
            }
        }
    }
    if (lpicurl == ""){
        lpicurl = "./images/my/touxiang.png"
    }
    var user = JSON.parse(localStorage.getItem("user"));
    user.uphoto = lpicurl;
    localStorage.setItem("user", JSON.stringify(user))
    //总的：设置头像
    $("#tx").attr('src',lpicurl);

}

function kfz(){
    $.alert("功能开发中...");
}

//加载我的-我的服务工单数量
function loadMyServiceNum() {
    //加载待服务工单数量
    Mdp.doAjaxData({
        model: "efamily_app",
        data: 'data_fw_list_sum_dfw',
        async: false,
    }, function (obj) {
        var num = obj.data.sum1;
        $("#dfw").text(num);
    });
    //加载服务中工单数量
    Mdp.doAjaxData({
        model: "efamily_app",
        data: 'data_fw_list_sum',
        fgdzt: 22,
        ffwlx: 1,
        async: false,
    }, function (obj) {
        var num = obj.data.sum1;
        $("#fwz").text(num);
    });

    //加已待服务工单数量
    Mdp.doAjaxData({
        model: "efamily_app",
        data: 'data_fw_list_sum',
        fgdzt: 99,
        ffwlx: 1,
        async: false,
    }, function (obj) {
        var num = obj.data.sum1;
        $("#yfw").text(num);
    });

}


//展示我的需求的数量
function showNumber(xywzt) {
    var user = JSON.parse(localStorage.getItem("user"));
    Mdp.doAjaxMgr({
        model: 'toBeResolveManager', method: 'get_toBeResolve',
        userid: user.userid, xywzt: '0,1' }, function (obj) {
        $("#toBeResolve").html((obj.data.data_toBeResolve == "") ? 0 : obj.data.data_toBeResolve);
    });

    Mdp.doAjaxMgr({
        model: 'toBeResolveManager', method: 'get_sum',
        userid: user.userid, ywzt: '2'}, function (obj) {
        $("#helping").html((obj.data.data_sum == "") ? 0 : obj.data.data_sum);
    });

    Mdp.doAjaxMgr({
        model: 'toBeResolveManager', method: 'get_sum',
        userid: user.userid, ywzt: '4' }, function (obj) {
        $("#completed").html((obj.data.data_sum == "") ? 0 : obj.data.data_sum);
    });

}