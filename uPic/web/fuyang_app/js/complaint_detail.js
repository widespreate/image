var pk_id = Mdp.getCurrParams().filter.pk_id;
console.log(pk_id)
$(function () {
    //判断是否登陆
    isLogin();
    getInfo();
})

//判断是否登录（未登陆的直接跳转到登陆页面）
function isLogin() {
    console.log('1')
    var flag = Mdp.isRemoteLogin();
    if (flag === false) {
        $.confirm('您还未登录，是否选择登录？', function () {
            localStorage.setItem("is_qrcode", "true");
            Mdp.openPage('../fuyang_app/login.html');
        }, function () {
            Mdp.openPage('../fuyang_app/index.html')
        });
    }
    return flag;
}

//加载页面信息
function getInfo() {
    Mdp.doAjaxRowData({
        model: 'efamily_app', data: 'data_complaintDetails',
        pk_id: pk_id, async: false
    }, function (obj) {
        var data = obj.data_complaintDetails.list[0];
        $("#complainant_name").html(data.complainant_name);
        $("#complainant_tel").html(data.complainant_tel);
        $("#sys_shrq").html(data.sys_shrq);
        $("#respondent_name").html(data.respondent_name);
        $("#sys_createtime").html(data.sys_createtime);
        $("#cause").html(data.cause);
        $("#sys_spztms").html(data.sys_spztms);
        var res = data.sys_spzt;
        if (res === '0') {
            $('#result').hide();
        }
    })
}


