$(function () {
    isLogin();
    initList();
});

function initList() {
    var params = {};
    params.model = "efamily_app";
    params.list = "gyct_query_list";
    // params.where = {"userid":userid};
    Mdp.doAjaxListPage(params, function (record, params) {
        console.log(params.dataset)
        if (params.dataset.list.length == 0) {
            return;
        }
    }, function (params) {
        if (params.dataset.list.length > 0) {
            $(".listNothing").css({display: "none"});
        }
    })
}
function isLogin() {
    var flag = Mdp.isRemoteLogin();
    if (flag == false) {
        $.confirm('您还未登录，是否选择登录？', function () {
            localStorage.setItem("is_qrcode", "true");
            Mdp.openPage('../fuyang_app/login.html');
        }, function () {
            Mdp.openPage('../fuyang_app/index.html')
        });
    }
    return flag;
}
