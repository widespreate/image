
let state = "";
let color = "";
let method = "";
let jsfs = "";
var user = JSON.parse(localStorage.getItem("user"));
let htmlEle = ``;

$(function (params) {
        console.log("==========");

        Mdp.doAjaxListPage({
            model: "yl_sjyh_app",
            list: "list_allhd"

        }, function (record, params) {
            console.log(params)
           /* let data = record.data_sjyh_home_activity.list;

            if ( data.length == 0) {
                document.getElementById("homeActivity").innerHTML= "<img src='../yl_sjyh_app/images/noplay2.png'>";
                return;
            }

            for(var i = 0; i < data.length; i++){
                let url = '';

                if (data[i].sys_spdj == "1") {
                    url = "activity-info-zmz.html?pk_id=" + data[i].pk_sr_zyzhd + "&page=1";
                } else {
                    url = "activity-info-detail.html?pk_id=" + data[i].pk_sr_zyzhd;
                }

                //活动描述
                let hdms="";
                switch(data[i].sys_spdj){
                    case "0":
                        hdms = "报名尚未开始";
                        break;
                    case "1":
                        hdms = "报名中";
                        break;
                    case "2":
                        hdms = "活动未开始";
                        break;
                    case "3":
                        hdms = "活动进行中";
                        break;
                    case "4":
                        hdms = "活动结束";
                        break;
                    default:
                        hdms = "活动尚未开放";
                }

                let zzpurl = data[i].zzpurl === "" ? "./images/public-info.png" : data[i].zzpurl;

                htmlEle += `
                    <a href="`+ url+`">
                     <div class="item"  >
                        <div class="item-top">
                            <div class="img-box" style="background-image: url(`+ zzpurl +`);background-size:cover;box-shadow: 1px 1px 3px #a19f9f;">
<!--                                <img src="./images/public-info.png" alt="">-->

                            <div class="bg">` + hdms + `</div>
                            </div>
                            <div class="item-info">
                                <p>` + data[i].hdmc + `</p>
                                <div class="line-row">负责人：<span>`+ data[i].hdfzr +`</span></div>
                                <div class="line-row">联系方式：<span style="color: #1A63FF;">`+ data[i].hdlxdh +`</span></div>
                                <div class="line-row">开始时间：<span>`+ data[i].hdkssj +`</span></div>
                            </div>
                        </div>
                    </div>
                    </a>
                `;
            }
            document.getElementById("homeActivity").innerHTML = htmlEle;*/





        }, function (params) {
            if ($(".listNothing").size() > 0) {
                $(".listNothing").css({display: "none"});
            }
        });


    })

function becomeAVolunteer() {
    var flag = Mdp.isRemoteLogin();

    if (flag == "" || flag == "null") {

        $.confirm('您还未登录，是否选择登录？', function () {
            Mdp.openPage('../fuyang_app/login.html');

        }, function () {

        })
    } else {
        if (user.utype != '1301') {
            $.alert('您不是个人用户！');
            return;
        }
        if (user.idcard == '' || user.idcard == undefined) {
            $.confirm('请先完成实名认证！',function () {
                Mdp.openPage('../fuyang_app/my.html')
            });
            return;
        }
        if (Mdp.pageStorage().getItem('zyzpk') !== null) {
            $.alert('您已是志愿者！');
            return;
        }
        becomeAVolunteerfunc();
    }
}

function rank() {
    var flag = Mdp.isRemoteLogin();

    if (flag == "" || flag == "null") {

        $.confirm('您还未登录，是否选择登录？', function () {
            Mdp.openPage('../fuyang_app/login.html');

        }, function () {

        })
    } else {
        if (user.utype != '1301') {
            $.alert('您不是个人用户！');
            return;
        }
        Mdp.openPage('integral_ranking_list.html')
    }
}
function toComplaint() {
    var flag = Mdp.isRemoteLogin();

    if (flag == "" || flag == "null") {

        $.confirm('您还未登录，是否选择登录？', function () {
            Mdp.openPage('../fuyang_app/login.html');

        }, function () {

        })
    } else {
        if (user.utype != '1301') {
            $.alert('您不是个人用户！');
            return;
        }
        Mdp.openPage('../cishan_app/complaint.html')
    }
}

function jif() {
    var flag = Mdp.isRemoteLogin();

    if (flag == "" || flag == "null") {

        $.confirm('您还未登录，是否选择登录？', function () {
            Mdp.openPage('../fuyang_app/login.html');

        }, function () {

        })
    } else {
        if (user.utype != '1301') {
            $.alert('您不是个人用户！');
            return;
        }
        Mdp.openPage('../yl_sjyh_app/integral-shopping-mall.html', {filter:{'page':'../yl_sjyh_app/home.html'}})
    }
}

function becomeAVolunteerfunc() {
    Mdp.doAjaxMgr({model:"userProcessing", method:"becomeAVolunteer"}, function (obj) {
        if (obj.success === 'true') {
            Mdp.doAjaxRowData({model: "yl_sjyh_app", data: "data_identity", fk_identity: user.userid},function (obj) {
                if (obj.success == "true") {
                    console.log(obj)
                    var infos = obj.data_identity.list;
                    for (var i = 0; i<infos.length; i++) {
                        var info = infos[i];
                        if (info.tname_id == "1") {
                            localStorage.setItem("zyzpk", info.ubm);
                            $.alert("恭喜您成为志愿者！");
                        }
                    }
                }
            })
        }
    })
}